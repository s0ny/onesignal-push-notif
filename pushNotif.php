<?php

require __DIR__ . '/oneSignal.php';

$data = [
    'contents' => [
        'en' => 'Test Push Notification message'
    ],
    'headings' => [
        'en' => 'Incoming call'
    ],
    // 'included_segments' => [
    //     'external_id' => explode(',', $_GET['targets']),
    // ],
    'include_external_user_ids' => explode(',', $_GET['targets']),
    "target_channel" => "push",
    "android_channel_id" => '75121ad0-e5fe-48ec-8aa4-1e770b5f0840',
    'isAndroid' => true,
    'isHuawei' => true,
    'buttons' => [['id' => 'hangup', 'text' => 'HANGUP'], ['id' => 'answer', 'text' => 'ANSWER']]
];

var_dump($data);

print_r($oneSignal->notifications()->add($data));
