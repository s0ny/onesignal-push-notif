<?php

declare(strict_types=1);

use OneSignal\Config;
use OneSignal\OneSignal;
use Symfony\Component\HttpClient\Psr18Client;
use Nyholm\Psr7\Factory\Psr17Factory;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

$config = new Config($config['APP_ID'], $config['APP_AUTH_KEY'], $config['AUTH_KEY']);
$httpClient = new Psr18Client();
$requestFactory = $streamFactory = new Psr17Factory();

$oneSignal = new OneSignal($config, $httpClient, $requestFactory, $streamFactory);
